Manual Testing
==============

MultiBrowser gives you all the necessary tools to run manual tests against all major desktop and mobile browsers. For this 4 different functions are available:

Live Testing
------------

Live testing is useful to quickly test or debug with a specific browser version. For this MultiBrowser will launch the browser you selected directly on your local machine. 

.. image:: _static/live.png

To start a new live test select **Live Testing** in the top navigation, enter the URL to test and select the browser version. After a click to **Start Test** the browser will be downloaded (if necessary) and launched on your machine.

You now have the unmodified release version of the the browser running on your local machine. This browser can be used for testing / development and includes the normal browser developer tools.

Browser Compare
---------------

For running through a regression testing script MultiBrowser provides you with the browser compare feature. With browser compare you can start up to 4 different browsers side-by-side and have your input synchronized across all different browsers. This can cut down your testing time significantly.

.. image:: _static/compare.png

To start a new compare test select **Compare** in the top navigaton, enter the URL to test and select up to 4 different browsers. After a click to **Start Test** all selected browser will be downloaded (if necessary) and launched.

For the best testing experience wait until all browser have finished loading to start your test. Your input (scrolling, keyboard input, clicking) will be synchronized across all open browsers.

Visual Testing
-----------

To find issues with the layout of a specific page you can run a visual tests. During this test MultiBrowser will create a **full page screenshot** and **automated layout comparison** in all selected browsers for you to inspect. 

.. image:: _static/screenshots.png

To start a new visual test select **Visual Testing** from the top navigation, enter the URL to test and select as many browsers as necessary. After a click to **Start Test** screenshots and layout comparisons for all selected browsers are created. 

.. note::
    
    After a visual test you can find all screenshots as image files in the subfolder *MultiBrowser* of your *Documents* folder.
    
Responsive Testing
-----------

To find issues with the layout in different desktop and mobile resolutions you can run a responsive tests. During this test MultiBrowser will create **full page screenshots** in all selected resolutions for you to inspect. 

.. image:: _static/responsive.png

To start a new responsive test select **Responsive Testing** from the top navigation, enter the URL to test and select as many options as necessary. After a click to **Start Test** screenshots for all selected options are created. 

.. note::
    
    After a responsive test you can find all screenshots as image files in the subfolder *MultiBrowser* of your *Documents* folder.

Page Analytics
--------------

This feature can help find errors and possible compatibility problems directly in the source code of a web page. For this MultiBrowser will analyse the HTML, CSS and JavaScript of a specific page and alert for any syntax errors, missing files and compatibility issues. 

.. image:: _static/analytics.png

To start a new analytics test select **Page Analytics** from the top navigation, enter the URL to test and select as many browsers as necessary. After a click to **Generate Analytics** the results will be displayed like this:

.. image:: _static/analytics_results.png