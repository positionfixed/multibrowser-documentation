MultiBrowser Documentation
=====================================================

`Homepage <https://www.MultiBrowser.com>`_

MultiBrowser is a client side cross browser testing tool. It runs natively on Windows, macOS and Linux and can be used to test for all major desktop and mobile browsers. You can use it directly on your local machine and in your local network.

.. toctree::
   :maxdepth: 2

   installation
   manual
   automation
   settings
   commandline